/*
Copyright 2016 Vinay Penmatsa

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.antosara.akandaka.model;

import java.io.Serializable;

/**
 *
 * @author Vinay Penmatsa
 */
public class Item implements Serializable {

    private String title;
    private String userName;
    private String password;
    private String url;
    private String misc;

    public Item() {

    }

    public Item(String title, String userName, String password, String url, String misc) {
        this.title = title;
        this.userName = userName;
        this.password = password;
        this.url = url;
        this.misc = misc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc;
    }

}
