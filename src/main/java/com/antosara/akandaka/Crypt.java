/*
Copyright 2016 Vinay Penmatsa

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.antosara.akandaka;

import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.logging.Logger;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.modes.GCMBlockCipher;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.Arrays;

/**
 *
 * @author Vinay Penmatsa
 * 
 */
public class Crypt {

    private static final Logger LOGGER = Logger.getLogger(Crypt.class.getName());
    
    private static final int NONCE_BIT_SIZE = 128;
    private static final int MAC_BIT_SIZE = 128;
    
    private final byte[] key;
    
    public Crypt(char[] password, byte[] salt) throws InvalidKeySpecException, NoSuchAlgorithmException {
        PBEParametersGenerator generator = new PKCS5S2ParametersGenerator();
        generator.init(
                PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(password), 
                salt, 100000);
        KeyParameter params = (KeyParameter) generator.generateDerivedParameters(256);
        key = params.getKey();
    }
    
    public String encrypt(String plainText) throws IllegalStateException, InvalidCipherTextException {
        byte[] plainBytes = plainText.getBytes(Charset.forName("UTF-8"));
        SecureRandom sr = new SecureRandom();
        byte[] nonce = new byte[NONCE_BIT_SIZE/8];
        sr.nextBytes(nonce);
        GCMBlockCipher gbc = new GCMBlockCipher(new AESEngine());
        AEADParameters params = new AEADParameters(new KeyParameter(key), MAC_BIT_SIZE, nonce);
        gbc.init(true, params);
        
        byte[] output = new byte[gbc.getOutputSize(plainBytes.length)];
        int len = gbc.processBytes(plainBytes, 0, plainBytes.length, output, 0);
        int finalLen = gbc.doFinal(output, len);
        
        byte[] all = new byte[output.length + nonce.length];
        System.arraycopy(nonce, 0, all, 0, nonce.length);
        System.arraycopy(output, 0, all, nonce.length, output.length);
        return Base64.getEncoder().encodeToString(all);
    }
    
    public String decrypt(String cipherText) throws IllegalStateException, InvalidCipherTextException {
        byte[] cipherBytes = Base64.getDecoder().decode(cipherText);
        byte[] nonce = Arrays.copyOfRange(cipherBytes, 0, NONCE_BIT_SIZE/8);
        byte[] rest = Arrays.copyOfRange(cipherBytes, NONCE_BIT_SIZE/8, cipherBytes.length);

        GCMBlockCipher gbc = new GCMBlockCipher(new AESEngine());
        byte[] output = new byte[rest.length - MAC_BIT_SIZE/8];
        AEADParameters params = new AEADParameters(new KeyParameter(key), MAC_BIT_SIZE, nonce);
        gbc.init(false, params);
        int len = gbc.processBytes(rest, 0, rest.length, output, 0);
        gbc.doFinal(output, len);
        
        return new String(output, Charset.forName("UTF-8"));
    }

    public static byte[] generateSalt() {
        byte[] salt = new byte[128];
        SecureRandom sr = new SecureRandom();
        sr.nextBytes(salt);
        return salt;
    }
    
    public void destroy() {
        Arrays.fill(key, (byte)0);
    }
}
