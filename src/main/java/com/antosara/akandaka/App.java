/*
Copyright 2016 Vinay Penmatsa

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.antosara.akandaka;

import com.antosara.akandaka.model.Database;
import com.antosara.akandaka.model.Item;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.bouncycastle.crypto.InvalidCipherTextException;

/**
 *
 * @author Vinay Penmatsa
 */
public class App implements ClipboardOwner {

    private static final String OPTION_CREATE_FILE = "c";
    private static final String OPTION_OPEN_FILE = "o";
    private static final String OPTION_LIST = "l";
    private static final String OPTION_ADD = "a";
    private static final String OPTION_DELETE = "d";
    private static final String OPTION_GET = "g";
    private static final String OPTION_EXIT = "e";
    private static final String OPTION_HELP = "h";

    private static final String VERIFICATION_DUMMY = "DUMMY";

    private Crypt crypt;
    private Database database;
    private String dbFilePath;
    private final Timer timer = new Timer(true);
    private static final String userHome = System.getProperty("user.home");

    public App() {

    }

    public void run() {
        CommandLineParser parser = new DefaultParser();
        Options options = buildOptions();

        Console console = System.console();
        console.printf("#################################\n");
        console.printf("*** Akandaka Secret Store ***\n");
        console.printf("*** Copyright (C) 2016 Vinay Penmatsa ***\n");
        console.printf("Licensed under the Apache License, Version 2.0 (the \"License\");\n"
                + "you may not use this file except in compliance with the License.\n"
                + "You may obtain a copy of the License at\n"
                + "\n"
                + "http://www.apache.org/licenses/LICENSE-2.0\n"
                + "\n"
                + "Unless required by applicable law or agreed to in writing, software\n"
                + "distributed under the License is distributed on an \"AS IS\" BASIS,\n"
                + "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n"
                + "See the License for the specific language governing permissions and\n"
                + "limitations under the License.\n");
        console.printf("#################################\n\n");

        Path akandakaFilePath = Paths.get(userHome, ".akandaka");
        File akandakaFile = akandakaFilePath.toFile();

        if(akandakaFile.exists()) {
            Properties props = new Properties();
            try {
                props.load(new FileInputStream(akandakaFile));
                if(props.containsKey("last_pwds_file")) {
                    String lastPwdsFile = props.getProperty("last_pwds_file");
                    if(new File(lastPwdsFile).exists()) {
                        console.printf("Shall I open the file you've used last time at '" + lastPwdsFile + "'? (y/n): ");
                        String input = console.readLine();
                        if ("y".equalsIgnoreCase(input)) {
                            openFile(console, lastPwdsFile);
                        }
                    }
                }
            } catch (IOException e) {
                // ignore
            }
        }

        while (true) {
            try {
                console.printf("%s ", ">");
                String input = console.readLine();
                String[] args = input.split("\\s");

                //===his for cli to work=====
                if (args[0].length() == 1) {
                    args[0] = "-" + args[0];
                } else {
                    args[0] = "--" + args[0];
                }
                //===========================

                try {
                    CommandLine cmd = parser.parse(options, args);
                    if (cmd.hasOption(OPTION_CREATE_FILE)) {
                        createFile(console, cmd);
                    } else if (cmd.hasOption(OPTION_OPEN_FILE)) {
                        openFile(console, cmd);
                    } else if (cmd.hasOption(OPTION_LIST)) {
                        list(console, cmd);
                    } else if (cmd.hasOption(OPTION_ADD)) {
                        add(console, cmd);
                    } else if (cmd.hasOption(OPTION_DELETE)) {
                        delete(console, cmd);
                    } else if (cmd.hasOption(OPTION_GET)) {
                        display(console, cmd);
                    } else if (cmd.hasOption(OPTION_EXIT)) {
                        if (crypt != null && database != null) {
                            try {
                                Helper.writeDatabase(database, dbFilePath);
                            } catch (IOException ex) {
                                console.printf("Could not save");
                            }
                            crypt.destroy();
                        }
                        console.printf("%s\n", "Bye.");
                        break;
                    } else {
                        printHelp(console, options);
                    }
                } catch (ParseException ex) {
                    console.printf("%s\n", "Invalid option");
                    printHelp(console, options);
                }
            } catch (Throwable t) {
                console.printf("Something terrible happened: " + t.getMessage());
            }
        }

    }

    private void createFile(Console console, CommandLine cmd) {
        String fileName;
        try {
            fileName = String.valueOf(cmd.getParsedOptionValue(OPTION_CREATE_FILE));
        } catch (ParseException ex) {
            console.printf(ex.getMessage());
            return;
        }
        Path newFilePath = Paths.get(fileName);
        if (Files.exists(newFilePath, LinkOption.NOFOLLOW_LINKS)) {
            console.printf("%s\n", "File exists. Choose another path");
            return;
        }

        console.printf("Enter password: ");
        char[] pw = console.readPassword();
        console.printf("Confirm password: ");
        char[] cpw = console.readPassword();
        if (!Arrays.equals(pw, cpw)) {
            console.printf("Password mismatch");
            return;
        }

        byte[] salt = Crypt.generateSalt();
        try {
            crypt = new Crypt(pw, salt);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
            console.printf(ex.getMessage() + "\n");
            return;
        }
        Arrays.fill(pw, '0');
        Arrays.fill(cpw, '0');
        database = new Database();
        database.setSalt(salt);

        try {
            database.setVerify(crypt.encrypt(VERIFICATION_DUMMY));
        } catch (IllegalStateException | InvalidCipherTextException ex) {
            crypt = null;
            console.printf(ex.getMessage() + "\n");
            return;
        }
        dbFilePath = newFilePath.toString();
        try {
            Helper.writeDatabase(database, dbFilePath);
        } catch (IOException ex) {
            crypt = null;
            database = null;
            dbFilePath = null;
            console.printf("%s\n", ex.getMessage() + ".Unable to create file. Try another path");

        }
    }

    private void openFile(Console console, String file) {
        Path filePath = Paths.get(file);
        if (!Files.exists(filePath, LinkOption.NOFOLLOW_LINKS)) {
            console.printf("%s\n", "File not found.");
            return;
        }

        dbFilePath = filePath.toString();
        if (crypt != null) {
            crypt.destroy();
            crypt = null;
        }

        console.printf("Enter password: ");
        char[] pw = console.readPassword();

        try {
            database = Helper.readDatabase(dbFilePath);
            crypt = new Crypt(pw, database.getSalt());
            Arrays.fill(pw, '0');
            String verificationStr = crypt.decrypt(database.getVerify());
            if (!verificationStr.equals(VERIFICATION_DUMMY)) {
                console.printf("Either password is wrong or database is tampered.Cannot continue.\n");
                dbFilePath = null;
                crypt = null;
                database = null;
            }
        } catch (IOException | ClassNotFoundException ex) {
            console.printf("Could not read " + dbFilePath + "\n");
            dbFilePath = null;
        } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
            console.printf("Could not create secret\n");
            dbFilePath = null;
            crypt = null;
            database = null;
        } catch (IllegalStateException | InvalidCipherTextException ex) {
            console.printf("Either password is wrong or database is tampered.Cannot continue.\n");
            dbFilePath = null;
            crypt = null;
            database = null;
        }

        Path akandakaFilePath = Paths.get(userHome, ".akandaka");
        Properties props = new Properties();
        try {
            props.setProperty("last_pwds_file", file);
            props.store(new FileOutputStream(akandakaFilePath.toFile()), "");
        } catch(Exception e) {
            console.printf("Error saving session: " + e.getMessage());
        }
    }

    private void openFile(Console console, CommandLine cmd) {
        String fileName;
        try {
            fileName = String.valueOf(cmd.getParsedOptionValue(OPTION_OPEN_FILE));
        } catch (ParseException ex) {
            console.printf(ex.getMessage());
            return;
        }

        openFile(console, fileName);
    }

    private void list(Console console, CommandLine cmd) {
        if (crypt == null || database == null) {
            console.printf("No database open\n");
            return;
        }

        List<Item> items = database.getItems();
        if (items == null || items.isEmpty()) {
            console.printf("No entries found.\n");
            return;
        }

        String filter = null;
        try {
            filter = String.valueOf(cmd.getParsedOptionValue(OPTION_LIST));
            if (filter != null) {
                if ("*".equals(filter.trim()) || filter.trim().isEmpty() || "null".equals(filter.trim())) {
                    filter = null;
                } else {
                    filter = filter.toLowerCase();
                }
            }
        } catch (ParseException ex) {
            console.printf("Invalid list filter argument" + ex.getMessage());
        }

        List<String> names = new ArrayList<>(items.size());

        try {
            for (Item item : items) {
                String title = crypt.decrypt(item.getTitle());
                if (filter == null) {
                    names.add(title);
                } else if (title != null && title.toLowerCase().contains(filter)) {
                    names.add(title);
                }
            }
        } catch (IllegalStateException | InvalidCipherTextException ex) {
            console.printf("Unable to decrypt: " + ex.getMessage());
        }

        Collections.sort(names);

        for (String name : names) {
            console.printf(name + "\n");
        }
        console.printf("\n");
    }

    private void add(Console console, CommandLine cmd) {
        if (crypt == null || database == null || dbFilePath == null) {
            console.printf("No database open\n");
            return;
        }

        console.printf("Title: ");
        String title = console.readLine();
        title = title.replace(' ', '_');
        List<Item> items = database.getItems();
        if (items != null && !items.isEmpty()) {
            try {
                for (Item item : items) {
                    String t = crypt.decrypt(item.getTitle());
                    if (t != null && t.equalsIgnoreCase(title)) {
                        console.printf("Entry with the same title exists\n");
                        return;
                    }
                }
            } catch (IllegalStateException | InvalidCipherTextException ex) {
                console.printf("Unable to search for existing titles: " + ex.getMessage());
            }
        }

        console.printf("User name:");
        String user = console.readLine();
        /*console.printf("Password:");
        String pw = console.readLine();*/
        console.printf("URL:");
        String url = console.readLine();
        console.printf("Notes:");
        String misc = console.readLine();
        
        console.printf("Enter password: ");
        char[] pw = console.readPassword();
        console.printf("Confirm password: ");
        char[] cpw = console.readPassword();
        if (!Arrays.equals(pw, cpw)) {
            console.printf("Password mismatch");
            return;
        }
        
        try {
            Item newItem = new Item(title, user, new String(pw), url, misc);
            database = Helper.createNewItem(newItem, database, crypt, dbFilePath);
        } catch (IllegalStateException | InvalidCipherTextException ex) {
            console.printf("Unable to create new item:" + ex.getMessage());
        } catch (IOException | ClassNotFoundException ex) {
            console.printf("Unable to create new item:" + ex.getMessage());
        }

    }

    private void display(Console console, CommandLine cmd) {
        if (crypt == null || database == null || dbFilePath == null) {
            console.printf("No database open\n");
            return;
        }

        String searchTitle = cmd.getOptionValue(OPTION_GET);

        if (searchTitle == null || searchTitle.trim().equals("null")
                || searchTitle.trim().isEmpty()) {
            console.printf("Invalid title/name");
            return;
        }

        List<Item> items = database.getItems();
        if (items != null && !items.isEmpty()) {
            try {
                for (Item item : items) {
                    String t = crypt.decrypt(item.getTitle());
                    if (t != null && t.equalsIgnoreCase(searchTitle)) {
                        console.printf("Title:%s\n", t);
                        console.printf("User name:%s\n", crypt.decrypt(item.getUserName()));
                        console.printf("Url:%s\n", crypt.decrypt(item.getUrl()));
                        console.printf("Notes:%s\n", crypt.decrypt(item.getMisc()));
                        console.printf("Password is in clipboard and is available for 5 seconds.\n");

                        Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
                        StringSelection stringSelection = new StringSelection(crypt.decrypt(item.getPassword()));
                        cb.setContents(stringSelection, null);

                        TimerTask cpDeleteTask = new TimerTask() {
                            @Override
                            public void run() {
                                cb.setContents(new Transferable() {
                                    @Override
                                    public DataFlavor[] getTransferDataFlavors() {
                                        return new DataFlavor[0];
                                    }

                                    @Override
                                    public boolean isDataFlavorSupported(DataFlavor df) {
                                        return false;
                                    }

                                    @Override
                                    public Object getTransferData(DataFlavor df) throws UnsupportedFlavorException, IOException {
                                        throw new UnsupportedFlavorException(df);
                                    }
                                }, null);
                            }
                        };
                        timer.schedule(cpDeleteTask, 5000);
                        return;
                    }
                }
            } catch (IllegalStateException | InvalidCipherTextException ex) {
                console.printf("Unable to search for existing titles: " + ex.getMessage());
            }
        }
    }

    private Options buildOptions() {
        Options options = new Options();
        options.addOption(Option.builder(OPTION_CREATE_FILE)
                .longOpt("create")
                .desc("Create file")
                .hasArg()
                .numberOfArgs(1)
                .argName("file path").build());
        options.addOption(Option.builder(OPTION_OPEN_FILE)
                .longOpt("open")
                .hasArg(true)
                .desc("Open file")
                .argName("file path").build());
        options.addOption(Option.builder(OPTION_LIST)
                .longOpt("list")
                .hasArg(true)
                .optionalArg(true)
                .desc("List entries")
                .argName("filter string").build());
        options.addOption(Option.builder(OPTION_ADD)
                .longOpt("add")
                .hasArg(false)
                .desc("Add new entry").build());
        options.addOption(Option.builder(OPTION_DELETE)
                .longOpt("delete")
                .hasArg(true)
                .desc("Delete entry").build());
        options.addOption(Option.builder(OPTION_GET)
                .longOpt("get")
                .hasArg(true)
                .numberOfArgs(1)
                .optionalArg(false)
                .desc("Display an entry and copy the password to clipboard")
                .argName("entry title").build());
        options.addOption(OPTION_HELP, "help", false, "Print help");
        options.addOption(OPTION_EXIT, "exit", false, "Exit");
        return options;
    }

    private void delete(Console console, CommandLine cmd) {
        if (crypt == null || database == null || dbFilePath == null) {
            console.printf("No database open\n");
            return;
        }

        String searchTitle = cmd.getOptionValue(OPTION_DELETE);

        if (searchTitle == null || searchTitle.trim().equals("null")
                || searchTitle.trim().isEmpty()) {
            console.printf("Invalid title/name");
            return;
        }

        List<Item> items = database.getItems();
        if (items != null && !items.isEmpty()) {
            try {
                for (int i = 0; i < items.size(); i++) {
                    String t = crypt.decrypt(items.get(i).getTitle());
                    if (t != null && t.equalsIgnoreCase(searchTitle)) {
                        items.remove(i);
                        try {
                            Helper.writeDatabase(database, dbFilePath);
                        } catch (IOException ex) {
                            console.printf("Unable to save db after delete: " + ex.getMessage());
                        }
                        break;
                    }
                }
            } catch (IllegalStateException | InvalidCipherTextException e) {
                console.printf("Unable to delete: " + e.getMessage());
            }
        }
    }

    private void printHelp(Console console, Options options) {
        console.printf("\n");
        for (Option opt : options.getOptions()) {
            console.printf("%s/%s\t<%s>\t:%s\n", opt.getOpt(), opt.getLongOpt(), opt.hasArg() ? opt.getArgName() : "", opt.getDescription());
        }
    }

    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    @Override
    public void lostOwnership(Clipboard clpbrd, Transferable t) {

    }

}
