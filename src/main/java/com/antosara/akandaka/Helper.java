/*
Copyright 2016 Vinay Penmatsa

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.antosara.akandaka;

import com.antosara.akandaka.model.Database;
import com.antosara.akandaka.model.Item;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Random;
import org.bouncycastle.crypto.InvalidCipherTextException;

/**
 *
 * @author Vinay Penmatsa
 */
public class Helper {

    private static final char[] PWCHARS = {
        '1', '2', '3', '4',
        'a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l',
        'm', 'n', 'o', 'p', 'q', 'r', 's',
        '~', '!', '@', '#', '$', '%',
        't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'J',
        'H', 'I', 'J', 'K', 'L',
        '^', '&', '-', '+', '=',
        'M', 'N', 'O', 'P', 'Q', 'R', 'S',
        'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        '5', '6', '7', '8', '9'
    };

    public static void writeDatabase(Database db, String path) throws IOException {
        File dbFile = new File(path);
        FileOutputStream fos = new FileOutputStream(dbFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(db);
    }

    public static Database readDatabase(String path) throws IOException, ClassNotFoundException {
        File dbFile = new File(path);
        FileInputStream fis = new FileInputStream(dbFile);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Database db = (Database) ois.readObject();
        return db;
    }

    public static Database createNewItem(Item newItem, Database db, Crypt crypt, String path) throws IllegalStateException, InvalidCipherTextException, IOException, ClassNotFoundException {
        Item cryptItem = new Item();
        cryptItem.setTitle(crypt.encrypt(newItem.getTitle()));
        cryptItem.setUserName(crypt.encrypt(newItem.getUserName()));
        cryptItem.setPassword(crypt.encrypt(newItem.getPassword()));
        cryptItem.setUrl(crypt.encrypt(newItem.getUrl()));
        cryptItem.setMisc(crypt.encrypt(newItem.getMisc()));
        db.addItem(cryptItem);
        writeDatabase(db, path);
        return readDatabase(path);
    }


    public static char[] generate(int len) {
        Random random = new Random(Calendar.getInstance().getTimeInMillis());
        char[] pw = new char[len];
        for (int i = 0; i < len; i++) {
            pw[i] = PWCHARS[random.nextInt(PWCHARS.length)];
        }
        return pw;
    }

    public static boolean isNotEmpty(String s) {
        return s != null && s.trim().length() != 0;
    }
}
